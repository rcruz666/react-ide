import React from 'react';
import ReactDOM from 'react-dom';

import { createStore } from 'redux';
import reducer from './rootReducer';
import {Provider} from 'react-redux';
import App from './elementos/componentes/App';

import './basic-ui/css/simplegrid.css';
import './basic-ui/css/reset.css';
import './basic-ui/css/cores.css';
import './basic-ui/css/commons.css';

//import {fromJS} from "../node_modules/immutable/dist/immutable";

const initialState = undefined;

const store = createStore(reducer, initialState, window.devToolsExtension && window.devToolsExtension());

ReactDOM.render(
  <Provider store={store}>
    <App />
  </Provider>,
  document.getElementById('root')
);
