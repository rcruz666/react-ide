import React from 'react';
import PropTypes from 'prop-types';

import './PanelCard.css';

const PanelCard = ({title, children}) => (
    <div className="panel-card primary-text-color white-background elevated">
        <div className="panel-card__title">
            {title}
        </div>
        <div className="panel-card__content">
            {children}
        </div>
    </div>
);

PanelCard.propTypes = {
    title: PropTypes.string.isRequired
}

export default PanelCard;