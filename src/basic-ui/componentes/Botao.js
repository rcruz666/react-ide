import React from 'react';
import PropTypes from 'prop-types';

import './Botao.css';

function Botao({label, onClick}) {
    return (
        <button className="botao" onClick={onClick}>
            {label}
        </button>
    );
}

Botao.propTypes = {
    label: PropTypes.node.isRequired,
    onClick: PropTypes.func.isRequired
};

export default Botao;
