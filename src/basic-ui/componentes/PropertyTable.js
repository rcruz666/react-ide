import React, {Component} from 'react';
import PropTypes from 'prop-types';

import './PropertyTable.css';

class PropertyTable extends Component {

    static propTypes = {
        propriedades: PropTypes.arrayOf(PropTypes.shape({
            id: PropTypes.string,
            seletorName: PropTypes.element,
            seletorValue: PropTypes.element
        })).isRequired
    };

    render() {
        return (
            <table className="property-table">
                <tbody>
                {this.props.propriedades.map(({id, seletorName, seletorValue}) => {
                    return (
                        <tr className="property-table__row" key={id}>
                            <td className="property-table__cell">
                                <span>{seletorName}</span>
                            </td>
                            <td className="property-table__cell">
                                {seletorValue}
                            </td>
                        </tr>
                    )
                })}
                </tbody>
            </table>
        );
    }
}

export default PropertyTable;
