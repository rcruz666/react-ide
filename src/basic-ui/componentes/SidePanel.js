import React from 'react';
import PropTypes from 'prop-types';
import './SidePanel.css';

const SidePanel = ({className, titulo, exibirBotaoFechar, onClickFechar, children}) => (
	<div className={"side-panel dark-primary-color text-primary-color " + className }>



		<div className="side-panel__title">

			{titulo}

            { exibirBotaoFechar &&
				<button
					className="side-panel__btn-fechar"
					onClick={onClickFechar}>X</button> }
		</div>

		{children}

	</div>
);

SidePanel.propTypes = {
    className: PropTypes.string,
	titulo: PropTypes.string.isRequired,
	exibirBotaoFechar: PropTypes.bool,
	onClickFechar: PropTypes.func
};

export default SidePanel;