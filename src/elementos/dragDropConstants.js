export const DraggableTypes = {
    ELEMENTO: 'elemento'
}

export const DropTargetTypes = {
    CANVAS_VAZIO_PRINCIPAL: 'canvas_vazio_principal',
    CANVAS_VAZIO_DENTRO_DE_INSTANCIA: 'canvas_vazio_dentro_de_instncia',
    INSTANCIA_ELEMENTO: 'instancia_elemento'
}