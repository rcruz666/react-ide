import {Map, List, fromJS} from 'immutable';
import  uuid from 'uuid/v4';

import * as actionTypes from './actionTypes';

import {DropTargetTypes} from './dragDropConstants';

const initialPaleta = fromJS({

    "div": {
        name: 'div',
        tag: 'div',
        container: true,
        estilosDefault: {
            display: 'flex',
            flex: 1,
            flexDirection: 'column'
        }
    },
    "span": {
        name: 'span',
        container: true,
        estilosDefault: {
            display: 'flex',
            flex: 1,
            flexDirection: 'row',
            alignItems: 'flex-start'
        }
    },
    "input text": {
        name: 'input text',
        tag: 'input',
        container: false,
        props: {
            type: 'text'
        },
        estilosDefault: {
            backgroundColor: "#f5ff83",
            color: "#000"
        }
    },
    "img": {
        name: "img",
        tag: 'img',
        container: false,
        props: {
            src: "https://upload.wikimedia.org/wikipedia/en/8/86/Avatar_Aang.png"
        },
        estilosDefault: {

        }
    }

});

const initialState = fromJS({
    elementoSelecionado: elementoSelecionado(undefined),
    idInstanciaEmEdicao: idInstanciaEmEdicao(undefined),
    dropTargetSelecionado: dropTargetSelecionado(undefined),
    paleta: paleta(undefined),
    canvas: canvas(undefined)
});

function criarInstanciaElemento(elemento) {

    const idInstancia = uuid();

    return new Map({
        idInstancia: idInstancia,
        tipoElemento: elemento.get('name'),
        children: new List(),
        estilo: elemento.get('estilosDefault') || new Map(),
        elemento
    });
}

//
// Reducers
//

function idInstanciaEmEdicao(state = null, action) {
    if (typeof action === 'undefined') return state;

    switch (action.type) {

        case actionTypes.SELECIONAR_INSTANCIA_PARA_EDICAO:
            return action.idInstancia;

        case actionTypes.CANCELAR_EDICAO_INSTANCIA:
            return null;

        default:
            return state;
    }
}

function elementoSelecionado(state = null, action) {

    if (typeof action === 'undefined') return state;

    switch (action.type) {

        case actionTypes.SELECIONAR_ELEMENTO:
            return action.elemento;

        case actionTypes.ADICIONAR_ELEMENTO:

            return null;

        default:
            return state;
    }
}

function paleta(state = initialPaleta, action) {

    if (typeof action === 'undefined') return state;

    return state;
}

function canvas(state = fromJS({

    elementosNoTopo: [],
    elementosAdicionados: {},

}), action, elementoSelecionado, dropTargetSelecionado) {

    if (typeof action === 'undefined') return state;

    switch (action.type) {

        case actionTypes.EXCLUIR_INSTANCIA:

            return state
                .set('elementosAdicionados', canvasElementosAdicionados(
                    state.get('elementosAdicionados'),
                    action))
                .set('elementosNoTopo', canvasElementosNoTopo(
                    state.get('elementosNoTopo'),
                    action));

        case actionTypes.ADICIONAR_ELEMENTO:

            // Não é possível adicionar um elemento ao canvas se não foi
            // selecionado nenhum elemento, e se não foi selecionado
            // nenhum drop target!
            if (!elementoSelecionado || !dropTargetSelecionado) return state;

            const novaInstancia = criarInstanciaElemento(elementoSelecionado);

            return state
                .set('elementosAdicionados', canvasElementosAdicionados(
                    state.get('elementosAdicionados'),
                    action,
                    novaInstancia,
                    dropTargetSelecionado))
                .set('elementosNoTopo', canvasElementosNoTopo(
                    state.get('elementosNoTopo'),
                    action,
                    novaInstancia,
                    dropTargetSelecionado));

        case actionTypes.SETAR_ESTILO_INSTANCIA_EM_EDICAO:

            return state
                .set('elementosAdicionados', canvasElementosAdicionados(
                    state.get('elementosAdicionados'),
                    action));

        default:
            return state;
    }
}

function canvasElementosNoTopo(state = List(), action, novaInstancia, dropTargetSelecionado) {

    if (typeof action === 'undefined') return state;

    switch (action.type) {

        case actionTypes.ADICIONAR_ELEMENTO:

            const tipoDropTarget = dropTargetSelecionado.get('tipo');
            const inseriuAoLadoDeUmaInstanciaNoTopoDoCanvas =
                tipoDropTarget === DropTargetTypes.INSTANCIA_ELEMENTO
                    && dropTargetSelecionado.get('idInstanciaPai') === null;

            // Se o drop target for o canvas principal , ou se não veio id de elemento pai, significa
            // que o elemento deve ser adicionado no array mais externo do canvas
            if ( tipoDropTarget === DropTargetTypes.CANVAS_VAZIO_PRINCIPAL
                    || inseriuAoLadoDeUmaInstanciaNoTopoDoCanvas ) {

                const indiceArray = action.indicePosicaoNovaInstancia;
                return state.splice(indiceArray, 0, novaInstancia.get('idInstancia'));
            }

            return state;

        case actionTypes.EXCLUIR_INSTANCIA:

            return state.filter(id => id !== action.idInstancia);

        default:

            return state;
    }

}

function canvasElementosAdicionados(state = Map(), action, novaInstancia, dropTargetSelecionado) {

    if (typeof action === 'undefined') return state;

    switch (action.type) {

        case actionTypes.ADICIONAR_ELEMENTO:

            const idNovaInstancia = novaInstancia.get('idInstancia');
            const newElementosAdicionados = state.set(idNovaInstancia, novaInstancia);

            const idInstanciaPai = dropTargetSelecionado.get('idInstanciaPai');

            if (idInstanciaPai) {
                // modifica o array children do elemento pai,
                // adicionado a chave do elemento selecionado
                // no índice indicado na action

                const elementoPai = newElementosAdicionados.get(idInstanciaPai);
                const childrenElementoPai = elementoPai.get('children');

                const indiceArray = action.indicePosicaoNovaInstancia;

                return newElementosAdicionados.merge({
                    [idInstanciaPai]: elementoPai.set('children',
                        childrenElementoPai.splice(indiceArray, 0, idNovaInstancia))
                });
            }

            return newElementosAdicionados;

        case actionTypes.SETAR_ESTILO_INSTANCIA_EM_EDICAO:

            const idInstanciaEmEdicao = action.idInstancia;
            const instancia = state.get(idInstanciaEmEdicao);

            if (action.valorEstilo === null) {
                return state.removeIn([idInstanciaEmEdicao, 'estilo', action.idEstilo]);
            }

            return state.merge({
                [idInstanciaEmEdicao]: instancia
                    .setIn(['estilo', action.idEstilo], action.valorEstilo)
            });

        case actionTypes.EXCLUIR_INSTANCIA:

            return state
                .delete(action.idInstancia)
                .map(instancia => instancia.set('children', instancia.get('children')
                    .filter(id => id !== action.idInstancia)));

        default:
            return state;
    }
}

function dropTargetSelecionado(state = null, action) {

    if (typeof action === 'undefined') return state;

    switch (action.type) {

        case actionTypes.SELECIONAR_DROP_TARGET:

            return new Map({
                tipo: action.tipoDropTarget,
                idInstancia: action.idInstancia,
                idInstanciaPai: action.idInstanciaPai
            });

        case actionTypes.ADICIONAR_ELEMENTO:

            return null;

        default:
            return state;
    }

}

export default function reducer(state = initialState, action) {

    if (typeof action === 'undefined') return state;

    switch (action.type) {

        case actionTypes.SELECIONAR_ELEMENTO:

            return state
                .set('elementoSelecionado', elementoSelecionado(state.get('elementoSelecionado'), action));

        case actionTypes.EXCLUIR_INSTANCIA:
        case actionTypes.ADICIONAR_ELEMENTO:
        case actionTypes.SETAR_ESTILO_INSTANCIA_EM_EDICAO:

            return state
                .set('elementoSelecionado', elementoSelecionado(state.get('elementoSelecionado'), action))
                .set('dropTargetSelecionado', dropTargetSelecionado(state.get('dropTargetSelecionado'), action))
                .set('canvas', canvas(
                    state.get('canvas'),
                    action,
                    state.get('elementoSelecionado'),
                    state.get('dropTargetSelecionado')));

        case actionTypes.SELECIONAR_DROP_TARGET:

            return state
                .set('dropTargetSelecionado', dropTargetSelecionado(state.get('dropTargetSelecionado'), action));

        case actionTypes.SELECIONAR_INSTANCIA_PARA_EDICAO:
        case actionTypes.CANCELAR_EDICAO_INSTANCIA:
            
            return state
                .set('idInstanciaEmEdicao', idInstanciaEmEdicao(state.get('idInstanciaEmEdicao'), action));

        

        default:
            return state;
    }
}