import ColorPicker from 'rc-color-picker';
import 'rc-color-picker/assets/index.css';

const UnidadesMedida = {
    PX: 'px',
    PORCENTO: '%',
    EM: 'em'
};

const ValueTypes = {
    STRING: 'STRING',
    NUMERICO_SIMPLES: 'NUMERICO_SIMLES',
    NUMERICO: 'NUMERICO'
};

const isValorNumerico = (val) => (
    typeof val === "object"
        && val.hasOwnProperty("type")
        && val.type === ValueTypes.NUMERICO
);

const valorNumericoToString = ({valor, unidadeMedida}) => valor + unidadeMedida;

const criarValorNumerico = (valor, unidadeMedida = null) => ({
    type: ValueTypes.NUMERICO,
    valor: Number(valor),
    unidadeMedida
});

/**
 * Retorna uma representação em string do valor css
 * @param val
 * @returns {*}
 */
export const CSSValueToString = (val) => {
    if (isValorNumerico(val)) {
        return valorNumericoToString(val);
    }

    return val;
};

/**
 * Retorna a porção do valor CSS que representa o valor
 * @param val
 * @returns {*}
 */
export const CSSValueValor = (val) => {
    if (isValorNumerico(val)) {
        return val.valor;
    }

    return val;
};

export const criarValorParaRegra = (regra, val) => {
    if (regra.tipoValor === ValueTypes.NUMERICO) {
        return criarValorNumerico(val, regra.unidadeMedida);
    }
    return val;
};

/**
 * Este array contém todas as propriedades CSS que se pode adicionar a uma
 * instância de elemento.
 *
 * Cada objeto contém obrigatoriamente uma propriedade "id", que deve
 * ser idêntica à string utilizada em um objeto style do React para representar
 * a propriedade CSS.
 *
 * Cada objeto pode conter também:
 *
 * - valoresPossíveis
 *   Um array de strings que define os valores que a propriedade CSS pode tomar.
 *   Se este membro estiver definido, o membro "componente" deve ficar undefined
 *
 * - componente
 *   Define um componente para ser utilizado como o seletor do valor da propriedade CSS.
 *   É um objeto com a seguinte forma:
 *   - nome: Pode ser uma string ou uma classe React
 *   - props: Objeto contendo props que o componente irá receber
 *   - makeOnChange: High Order Function que recebe um callback e retorna um onChangeHandler
 *                   especializado para o componente
 *   - valuePropName: String especificando qual a propriedade do componente que deve receber
 *                    o valor da propriedade CSS
 * - defaultValue
 *   Define um valor padrão para a propriedade CSS
 *
 *
 */
export const regrasCSS = [
    {
        id: "display",
        tipoValor: ValueTypes.STRING,
        valoresPossiveis: ["flex", "block", "inline", "inline-block"]
    },
    {
        id: "flex",
        tipoValor: ValueTypes.NUMERICO_SIMPLES,
        defaultValue: 1,
        componente: {
            nome: "input",
            props: {
                type: "number"
            }
        }
    },
    {
        id: "flexDirection",
        tipoValor: ValueTypes.STRING,
        valoresPossiveis: ["column", "column-reverse", "row", "row-reverse"]
    },
    {
        id: "justifyContent",
        tipoValor: ValueTypes.STRING,
        valoresPossiveis: ["center", "flex-end", "flex-start", "space-around", "space-between"]
    },
    {
        id: "alignItems",
        tipoValor: ValueTypes.STRING,
        valoresPossiveis: ["baseline", "center", "flex-end", "flex-start", "stretch"]
    },
    {
        id: "width",
        tipoValor: ValueTypes.NUMERICO,
        defaultValue: criarValorNumerico(100, UnidadesMedida.PX),
        unidadeMedida: UnidadesMedida.PX,
        componente: {
            nome: "input",
            props: {
                type: "number"
            }
        }
    },
    {
        id: "height",
        tipoValor: ValueTypes.NUMERICO,
        defaultValue: criarValorNumerico(100, UnidadesMedida.PX),
        componente: {
            nome: "input",
            props: {
                type: "number"
            }
        }
    },
    {
        id: "backgroundColor",
        tipoValor: ValueTypes.STRING,
        defaultValue: "#fff",
        componente: {
            nome: ColorPicker,
            valuePropName: "color",
            makeOnChange: (onChange) => ({color}) => onChange(color)
        }
    },
    {
        id: "color",
        tipoValor: ValueTypes.STRING,
        defaultValue: "#000",
        componente: {
            nome: ColorPicker,
            valuePropName: "color",
            makeOnChange: (onChange) => ({color}) => onChange(color)
        }
    }
];

