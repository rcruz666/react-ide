export const getIdElementoAdicionadoNaPosicao = (state, indice) => {
    const elemAdicionados = state.getIn(['elementos', 'canvas', 'elementosAdicionados'])
    return elemAdicionados.keySeq().get(indice)
}

export const getNomesElementosPaleta = (state) => {
    return state.getIn(['elementos', 'paleta']).keySeq()
}

export const getElementoPaletaByName = (state, nomeElemento) => {
    return state.getIn(['elementos', 'paleta']).get(nomeElemento);
}

export const getIdsInstanciasNoTopoDoCanvas = (state) => {
    return state.getIn(['elementos', 'canvas', 'elementosNoTopo']);
}

export const getIdsInstanciasByInstanciaPai = (state, idIntanciaPai) => {
    if (!idIntanciaPai) return getIdsInstanciasNoTopoDoCanvas(state);

    const elementosAdicionados = state.getIn(['elementos', 'canvas', 'elementosAdicionados']);
    const instancia = elementosAdicionados.get(idIntanciaPai);
    return instancia.get('children');
}

export const getInstanciaElementoById = (state, idInstanciaElemento) => {
    // debugger;
    const elemAdicionados = state.getIn(['elementos', 'canvas', 'elementosAdicionados'])
    return elemAdicionados.get(idInstanciaElemento)
}

export const getDropTargetSelecionado = (state) => {
    return state.getIn(['elementos', 'dropTargetSelecionado']);
}

export const getIdInstanciaEmEdicao = (state) => state.getIn(['elementos', 'idInstanciaEmEdicao']);

export const getInstanciaEmEdicao = (state) => {
    const id = getIdInstanciaEmEdicao(state);
    return state.getIn(['elementos', 'canvas', 'elementosAdicionados', id]);
}