
import {List, fromJS} from 'immutable';

import * as actions from '../actions';
import reducer from '../../rootReducer';
import * as selectors from '../selectors';

import * as testHelpers from './helpers';

import {DropTargetTypes} from '../dragDropConstants'

describe ('reducers/elementos', () => {

    //
    // Adição de elementos ao canvas
    //

    it (`deve definir um elemento como selecionado ao se selecionar 
        um elemento, passando um nome de elemento como parâmetro`, () => {

        const action = actions.selecionar('div');
        const estadoObtido = reducer(undefined, action);

        expect(estadoObtido.getIn(['elementos', 'elementoSelecionado'])).toEqual(fromJS({
            name: 'div'
        }));

    });

    it (`deve definir um elemento como selecionado ao se selecionar 
        um elemento, passando um elemento como parâmetro`, () => {

        const action = actions.selecionar(fromJS({
            name: 'div'
        }));
        const estadoObtido = reducer(undefined, action).get('elementos');

        expect(estadoObtido.get('elementoSelecionado')).toEqual(fromJS({
            name: 'div'
        }));

    });

    it (`deve adicionar uma div ao canvas`, () => {

        const estado = testHelpers.adicionarAoCanvas(undefined, 'div', 0).get('elementos');

        const elemAdicionados = estado.getIn(['canvas', 'elementosAdicionados']);

        expect(elemAdicionados.keySeq().count())
            .toEqual(1);
        expect(elemAdicionados.first().get('tipoElemento')).toEqual("div");
        expect(elemAdicionados.first().get('children')).toEqual(List());

        expect(estado.getIn(['canvas', 'elementosNoTopo']).count())
            .toEqual(1);
        expect(estado.get('elementoSelecionado')).toBeNull();
    });

    it (`deve adicionar duas divs ao canvas`, () => {

        const state1 = testHelpers.adicionarAoCanvas(undefined, 'div', 0);
        const state2 = testHelpers.adicionarAoCanvas(state1, 'div', 1).get('elementos');

        const elemAdicionados = state2.getIn(['canvas', 'elementosAdicionados']);

        expect(elemAdicionados.keySeq().count()).toEqual(2);

        expect(elemAdicionados.valueSeq().get(0).get('tipoElemento')).toEqual("div");
        expect(elemAdicionados.valueSeq().get(1).get('tipoElemento')).toEqual("div");

        expect(state2.getIn(['canvas', 'elementosNoTopo']).count()).toEqual(2);

        expect(state2.get('elementoSelecionado')).toBeNull();
    });


    it (`deve adicionar duas divs no canvas, e um span em cada div adicionada`, () => {

        // adiciona a primeira div ao canvas vazio
        let state = testHelpers.adicionarAoCanvas(undefined, 'div', 0);

        const idPrimeiraDiv = selectors.getIdElementoAdicionadoNaPosicao(state, 0);

        // adiciona a segunda div
        state = reducer(state, actions.selecionar('div'));
        state = reducer(state, actions.selecionarDropTargetInstanciaNoTopo(idPrimeiraDiv));
        state = reducer(state, actions.adicionarAoCanvas(1));

        const idSegundaDiv = selectors.getIdElementoAdicionadoNaPosicao(state, 1);

        // seleciona e adiciona o primeiro span
        state = testHelpers.adicionarElementoACanvasVazioEmElemento(state, 'span', idPrimeiraDiv);

        const idPrimeiroSpan = state.getIn([
            'elementos', 'canvas', 'elementosAdicionados', idPrimeiraDiv, 'children', 0]);

        expect(idPrimeiroSpan).not.toBeNull();

        // seleciona e adiciona o segundo span
        state = testHelpers.adicionarElementoACanvasVazioEmElemento(state, 'span', idSegundaDiv).get('elementos');

        const idSegundoSpan = state.getIn([
            'elementos', 'canvas', 'elementosAdicionados', idSegundaDiv, 'children', 0]);

        expect(idSegundoSpan).not.toBeNull();
    });

    //
    // Drop Target
    //

    it(`deve setar como drop target o canvas vazio principal da aplicação,
        e inserir o novo elemento`, () => {


        // Ação 1: Selecionar uma div

        const action1_selecionarDiv = actions.selecionar('div');
        // Uma div está selecionada
        const state1 = reducer(undefined, action1_selecionarDiv);

        // Ação 2: Selecionar como drop target o canvas vazio principal

        const action2_dropTargetCanvasPrincipal = actions.selecionarDropTargetCanvasVazioPrincipal();
        // Uma div está selecionada, e o drop target é o canvas vazio principal
        const state2 = reducer(state1, action2_dropTargetCanvasPrincipal);

        // Ação 3: Adicionar o elemento ao canvas
        const action3_adicionarElemAoCanvas = actions.adicionarAoCanvas();
        // A div adicionada ao canvas
        const state3 = reducer(state2, action3_adicionarElemAoCanvas).get('elementos');

        const elemAdicionados = state3.getIn(['canvas', 'elementosAdicionados']);

        // espera que haja 1 elemento adicionado ao canvas.
        expect(elemAdicionados.keySeq().count()).toEqual(1);

        // espera que o elemento adicionado ao canvas seja uma div
        expect(elemAdicionados.valueSeq().get(0).get('tipoElemento')).toEqual("div");

        // espera que o elemento selecionado esteja vazio
        expect(state3.get('elementoSelecionado')).toBeNull();

        // espera que o drop target selecionado esteja vazio
        expect(state3.get('dropTargetSelecionado')).toBeNull();
    });

    it(`deve setar como drop target uma instância de elemento que está no 
        topo do canvas, posicionando o novo elemento a ser criado antes dela,
        e inserir o novo elemento`, () => {


        // Adiciona uma div ao canvas
        let state = testHelpers.adicionarAoCanvas(undefined, 'div', 0);

        // Obtém o id da div adicionada (primeiro elemento no topo)
        const idInstancia = state.getIn(['elementos', 'canvas', 'elementosNoTopo', 0]);

        // Seleciona um span
        state = reducer(state, actions.selecionar('span'));

        // Seleciona a div adicionada anteriormente como drop target
        state = reducer(state, actions.selecionarDropTargetInstanciaNoTopo(idInstancia));

        // Adiciona o span
        state = reducer(state, actions.adicionarAoCanvas()).get('elementos');

        const elemAdicionados = state.getIn(['canvas', 'elementosAdicionados']);

        // espera que haja dois elementos no canvas
        expect(elemAdicionados.keySeq().count()).toEqual(2);

        // espera que id do elemento na primeira posição no canvas seja o do span
        expect(state.getIn(['canvas', 'elementosNoTopo', 0])).toEqual(
            elemAdicionados.find( val => val.get('tipoElemento') === 'span')
                .get('idInstancia')
        );

        // espera que id do elemento na segunda posição no canvas seja o da div
        expect(state.getIn(['canvas', 'elementosNoTopo', 1])).toEqual(
            elemAdicionados.find( val => val.get('tipoElemento') === 'div')
                .get('idInstancia')
        );

        // espera que o elemento selecionado esteja vazio
        expect(state.get('elementoSelecionado')).toBeNull();

        // espera que o drop target selecionado esteja vazio
        expect(state.get('dropTargetSelecionado')).toBeNull();

    });

    it(`deve setar como drop target uma instância de elemento que está no
        topo do canvas, posicionando o novo elemento a ser criado depois dela,
        e inserir o novo elemento`, () => {

        // Adiciona uma div ao canvas
        let state = testHelpers.adicionarAoCanvas(undefined, 'div', 0);

        // Obtém o id da div adicionada (primeiro elemento no topo)
        const idInstancia = state.getIn(['elementos', 'canvas', 'elementosNoTopo', 0]);

        // Seleciona um span
        state = reducer(state, actions.selecionar('span'));

        // Seleciona a div adicionada anteriormente como drop target, setando o índice de
        // inserção do elemento para depois dela (índice 1, pois ela está na posição 0)
        state = reducer(state, actions.selecionarDropTargetInstanciaNoTopo(idInstancia));

        // Adiciona o span
        state = reducer(state, actions.adicionarAoCanvas(1)).get('elementos');

        const elemAdicionados = state.getIn(['canvas', 'elementosAdicionados']);


        // espera que haja dois elementos no canvas
        expect(elemAdicionados.keySeq().count()).toEqual(2);

        // espera que id do elemento na primeira posição no canvas seja a da div
        expect(state.getIn(['canvas', 'elementosNoTopo', 0])).toEqual(
            elemAdicionados.find( val => val.get('tipoElemento') === 'div')
                .get('idInstancia')
        );

        // espera que id do elemento na segunda posição no canvas seja o do span
        expect(state.getIn(['canvas', 'elementosNoTopo', 1])).toEqual(
            elemAdicionados.find( val => val.get('tipoElemento') === 'span')
                .get('idInstancia')
        );

        // espera que o elemento selecionado esteja vazio
        expect(state.get('elementoSelecionado')).toBeNull();

        // espera que o drop target selecionado esteja vazio
        expect(state.get('dropTargetSelecionado')).toBeNull();

    });

    it(`deve setar como drop target um canvas vazio dentro de uma instância de
        elemento, e inserir o novo elemento`, () => {

        // Adiciona uma div ao canvas
        let state = testHelpers.adicionarAoCanvas(undefined, 'div', 0);

        // Obtém o id da div adicionada (primeiro elemento no topo)
        const idInstanciaDIV = state.getIn(['elementos', 'canvas', 'elementosNoTopo', 0]);

        // Seleciona um span
        state = reducer(state, actions.selecionar('span'));

        // Seleciona a div adicionada anteriormente como drop target, setando o índice de
        // inserção do elemento para depois dela (índice 1, pois ela está na posição 0)
        state = reducer(state, actions.selecionarDropTargetCanvasVazioInstancia(idInstanciaDIV));

        // Adiciona o span
        state = reducer(state, actions.adicionarAoCanvas()).get('elementos');

        const elemAdicionados = state.getIn(['canvas', 'elementosAdicionados']);

        const idInstanciaSPAN = elemAdicionados
            .find( val => val.get('tipoElemento') === 'span')
            .get('idInstancia');

        // espera que haja dois elementos no canvas
        expect(elemAdicionados.keySeq().count()).toEqual(2);

        // espera que haja apenas 1 elemento no topo do canvas
        expect(state.getIn(['canvas', 'elementosNoTopo']).keySeq().count()).toEqual(1);

        // espera que id do elemento na primeira posição no canvas seja a da div
        expect(state.getIn(['canvas', 'elementosNoTopo', 0])).toEqual(idInstanciaDIV);

        // espera que haja o id do span dentro do array de children da div
        expect(elemAdicionados
            .get(idInstanciaDIV)
            .getIn(['children', 0])
        ).toEqual(idInstanciaSPAN);

        // espera que o elemento selecionado esteja vazio
        expect(state.get('elementoSelecionado')).toBeNull();

        // espera que o drop target selecionado esteja vazio
        expect(state.get('dropTargetSelecionado')).toBeNull();

    });


    it(`deve setar como drop target uma instância de elemento que seja filha
        de outra instância, e inserir o novo elemento depois dela`, () => {


        // Adiciona uma div ao canvas
        let state = testHelpers.adicionarAoCanvas(undefined, 'div', 0);

        // seleciona e adiciona o primeiro span
        const idInstanciaDiv = state.getIn(['elementos', 'canvas', 'elementosNoTopo', 0]);
        state = testHelpers.adicionarElementoACanvasVazioEmElemento(state, 'span', idInstanciaDiv);

        const idInstanciaSPAN = state.getIn(['elementos', 'canvas', 'elementosAdicionados'])
            .find( val => val.get('tipoElemento') === 'span')
            .get('idInstancia');

        // Seleciona um input
        state = reducer(state, actions.selecionar('input'));

        // Seleciona o span adicionado anteriormente como drop target, setando o índice de
        // inserção do elemento para depois dele (índice 1, pois ele está na posição 0)
        state = reducer(state, actions.selecionarDropTargetInstancia(idInstanciaSPAN, idInstanciaDiv));

        // Adiciona o input
        state = reducer(state, actions.adicionarAoCanvas(1)).get('elementos');

        const idInstanciaInput = state.getIn(['canvas', 'elementosAdicionados'])
            .find( val => val.get('tipoElemento') === 'input')
            .get('idInstancia');

        const elemAdicionados = state.getIn(['canvas', 'elementosAdicionados']);
        const childrenDiv = elemAdicionados
            .get(idInstanciaDiv)
            .getIn(['children']);

        // espera que haja 3 elementos no canvas
        expect(elemAdicionados.keySeq().count()).toEqual(3);

        // espera que id do elemento na primeira posição no canvas seja a da div
        expect(state.getIn(['canvas', 'elementosNoTopo', 0])).toEqual(idInstanciaDiv);

        // espera que haja o id do span como primeiro elemento no array de children da div
        expect(childrenDiv.get(0)).toEqual(idInstanciaSPAN);

        // espera que haja o id do input como segundo elemento no array de children da div
        expect(childrenDiv.get(1)).toEqual(idInstanciaInput);

        // espera que o elemento selecionado esteja vazio
        expect(state.get('elementoSelecionado')).toBeNull();

        // espera que o drop target selecionado esteja vazio
        expect(state.get('dropTargetSelecionado')).toBeNull();

    });

});

