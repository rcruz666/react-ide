import {fromJS} from 'immutable';

import * as actions from '../actions';
import reducer from '../../rootReducer';
import {DropTargetTypes} from '../dragDropConstants';

/**
 *
 * @param lastState Estado anterior
 * @param tipoElemento Tipo do elemento que será adicionado ao outro elemento
 * @param idInstanciaPai Id do elemento no qual será adicionado um novo elemento
 * @param indice Índice do array children, no qual o novo elemento será inserido
 * @returns {*}
 */
export function adicionarElementoACanvasVazioEmElemento(lastState, tipoElemento, idInstanciaPai) {

    let state = reducer(lastState, actions.selecionar(tipoElemento));

    state = reducer (state, actions.selecionarDropTargetCanvasVazioInstancia(idInstanciaPai));

    return reducer(state, actions.adicionarAoCanvas());
}

/**
 * Adiciona um elemento ao canvas (no array mais externo de elementos do canvas)
 *
 * @param lastState Estado anterior
 * @param elemName Tipo do elemento que será adicionado
 * @param i Índice do array de elementos do canvas, no qual o elemento novo será inserido
 * @returns {*}
 */
export function adicionarAoCanvas(lastState = undefined, elemName, i) {

    // seleciona uma div para adicionar ao canvas
    const action1 = actions.selecionar(fromJS({
        name: elemName
    }));
    const estado1 = reducer(lastState, action1);

    // seleciona o canvas externo vazio como drop target
    const action2 = actions.selecionarDropTargetCanvasVazioPrincipal();
    const estado2 = reducer(estado1, action2);

    // adiciona a div selecionada no canvas
    const action3 = actions.adicionarAoCanvas(i);
    return reducer(estado2, action3);
}