import {connect} from 'react-redux'
import ElementPlaceholder from '../componentes/ElementPlaceholder'
import {getNomesElementosPaleta} from '../selectors'
import {adicionarAoCanvas} from '../actions'


const mapDispatchToProps = {
    adicionarAoCanvas
}

export default connect(null, mapDispatchToProps)(ElementPlaceholder)