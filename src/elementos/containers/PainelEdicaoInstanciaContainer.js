import {connect} from 'react-redux'
import PainelEdicaoInstancia from '../componentes/PainelEdicaoInstancia'
import {getIdInstanciaEmEdicao} from "../selectors";
import {cancelarEdicaoInstancia} from "../actions";

const mapStateToProps = (state) => ({
    visivel: getIdInstanciaEmEdicao(state) !== null
});

const mapDispatchToProps = (dispatch) => ({
    onClickFechar: () => dispatch( cancelarEdicaoInstancia() )
});

export default connect(mapStateToProps, mapDispatchToProps)(PainelEdicaoInstancia)