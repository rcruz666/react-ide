import {connect} from 'react-redux'
import InstanciaElemento from '../componentes/InstanciaElemento'
import {
    getInstanciaElementoById, 
    getDropTargetSelecionado,
    getIdInstanciaEmEdicao
} from '../selectors'

const mapStateToProps = (state, {idInstanciaElemento}) => ({
    instanciaElemento: getInstanciaElementoById(state, idInstanciaElemento),
    dropTargetSelecionado: getDropTargetSelecionado(state),
    emEdicao: getIdInstanciaEmEdicao(state) === idInstanciaElemento
});

export default connect(mapStateToProps)(InstanciaElemento)