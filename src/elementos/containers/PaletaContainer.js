import {connect} from 'react-redux'
import Paleta from '../componentes/Paleta'
import {getNomesElementosPaleta} from '../selectors'

const mapStateToProps = (state) => ({
    nomesElementos: getNomesElementosPaleta(state)
})

export default connect(mapStateToProps)(Paleta)