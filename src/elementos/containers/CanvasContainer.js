import {connect} from 'react-redux'
import Canvas from '../componentes/Canvas'
import {getIdsInstanciasByInstanciaPai, getDropTargetSelecionado} from '../selectors'

const mapStateToProps = (state, {idInstanciaPai}) => ({
    idInstanciaPai,
    idsInstanciasElementos: getIdsInstanciasByInstanciaPai(state, idInstanciaPai),
    dropTargetSelecionado: getDropTargetSelecionado(state)
});

export default connect(mapStateToProps)(Canvas)