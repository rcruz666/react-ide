import {connect} from 'react-redux'
import Elemento from '../componentes/Elemento'
import {getElementoPaletaByName} from '../selectors'
import {selecionar} from '../actions'

const mapStateToProps = (state, {nomeElemento}) => ({
    elemento: getElementoPaletaByName(state, nomeElemento)
})

const mapDispatchToProps = {
    selecionar
}

export default connect(mapStateToProps, mapDispatchToProps)(Elemento)