import {connect} from 'react-redux'
import TabelaEdicaoEstilos from '../componentes/TabelaEdicaoEstilos'
import {getInstanciaEmEdicao} from "../selectors";
import {setarEstiloInstanciaEmEdicao} from "../actions";

const mapStateToProps = (state) => ({
    instancia: getInstanciaEmEdicao(state)
});

const mapDispatchToProps = (dispatch) => ({
    onChangeEstilo: (idInstancia, idEstilo, valorEstilo) =>
        dispatch( setarEstiloInstanciaEmEdicao(idInstancia, idEstilo, valorEstilo) )
});

export default connect(mapStateToProps, mapDispatchToProps)(TabelaEdicaoEstilos)