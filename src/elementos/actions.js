import * as actionTypes from './actionTypes';
import {DropTargetTypes} from './dragDropConstants';

/**
 * @param elemento ImmutableMap Elemento
 */
export const selecionar = (elemento) => ({
    type: actionTypes.SELECIONAR_ELEMENTO,
    elemento
});

/**
 * Adiciona o elemento selecionado no canvas, de acordo com
 * o local / posição indicada no dropTargetSelecionado
 *
 * @param indicePosicaoNovaInstancia number|null Número indicando a posição no array
 *      em que será inserido o novo elemento.
 */
export const adicionarAoCanvas = (indicePosicaoNovaInstancia = 0) => ({
    type: actionTypes.ADICIONAR_ELEMENTO,
    indicePosicaoNovaInstancia
});

export const excluirInstancia = (idInstancia) => ({
    type: actionTypes.EXCLUIR_INSTANCIA,
    idInstancia
});

/**
 * Ação privada. Não é exposta pois espera vários parametros que podem
 * ficar confusos, por isso possui ações auxiliares que chamarão ela,
 * e são mais focadas, possuindo menos parâmetros.
 *
 * Seleciona uma instância de elemento ou um canvas vazio como o
 * drop target ativo no momento
 *
 * @param tipoDropTarget string Pode ser um dos tipos enumerados em
 *          elementos/dragDropConstants/DropTargetTypes
 * @param idInstancia string|null id da instância sobre qual o elemento está atualmente
 *          sendo arrastado. Se o elemento estiver sendo arrastado
 *          sobre um canvas vazio, este parâmetro fica nulo.
 * @param idInstanciaPai string|null id da instância que é pai da instância do
 *          parâmetro "idInstancia", ou seja, é o id da instância em cujo array
 *          "children" está o id do parâmetro "idInstancia"
 *
 */
const selecionarDropTarget = (
    tipoDropTarget,
    idInstancia = null,
    idInstanciaPai = null,
) => ({
    type: actionTypes.SELECIONAR_DROP_TARGET,
    tipoDropTarget,
    idInstancia,
    idInstanciaPai
})

/**
 * Seleciona como drop target o canvas vazio principal da aplicação.
 * O novo elemento será inserido no array de elementos no topo.
 */
export const selecionarDropTargetCanvasVazioPrincipal = () => selecionarDropTarget(DropTargetTypes.CANVAS_VAZIO_PRINCIPAL)

/**
 * Seleciona como drop target uma instância de elemento que esteja no topo do canvas.
 * O novo elemento será inserido como irmão da instancia drop target
 * @param idInstanciaNoTopo
 */
export const selecionarDropTargetInstanciaNoTopo = (idInstanciaNoTopo) =>
    selecionarDropTarget(
        DropTargetTypes.INSTANCIA_ELEMENTO,
        idInstanciaNoTopo,
        null
    )

/**
 * Seleciona como drop target uma instancia que não esteja no topo do canvas, ou seja,
 * que esteja aninhada dentro de outra instância.
 * O novo elemento será inserido como irmão do drop target.
 * @param idInstancia
 * @param idInstanciaPai
 */
export const selecionarDropTargetInstancia = (idInstancia, idInstanciaPai) =>
    selecionarDropTarget(
        DropTargetTypes.INSTANCIA_ELEMENTO,
        idInstancia,
        idInstanciaPai
    )

/**
 * Seleciona como drop target um canvas vazio dentro de algum instância.
 * O novo elemento será inserido como filho da instância.
 * @param idInstanciaPai
 */
export const selecionarDropTargetCanvasVazioInstancia = (idInstanciaPai) =>
    selecionarDropTarget(
        DropTargetTypes.CANVAS_VAZIO_DENTRO_DE_INSTANCIA,
        null,
        idInstanciaPai
    )

/**
 * Seleciona uma instância do canvas para edição. Um painel lateral 
 * deve aparecer ao lado, para que as propriedades da instância possam
 * ser editadas.
 * @param {string} idInstancia 
 */
export const selecionarInstanciaParaEdicao = (idInstancia) => ({
    type: actionTypes.SELECIONAR_INSTANCIA_PARA_EDICAO,
    idInstancia
})

/**
 * Fecha o painel de edição de instâncias
 */
export const cancelarEdicaoInstancia = () => ({
    type: actionTypes.CANCELAR_EDICAO_INSTANCIA
})

/** 
 * Esta ação é disparada quando o usuário troca alguma regra de estilo para o componente
 * em edição
 * @param {string} idInstancia
 * @param {string} idEstilo
 * @param {string} valorEstilo
 */
export const setarEstiloInstanciaEmEdicao = (idInstancia, idEstilo, valorEstilo) => ({
    type: actionTypes.SETAR_ESTILO_INSTANCIA_EM_EDICAO,
    idInstancia,
    idEstilo,
    valorEstilo
})