import React from 'react';
import PropTypes from 'prop-types';
import SidePanel from "../../basic-ui/componentes/SidePanel";
import TabelaEdicaoEstilosContainer from "../containers/TabelaEdicaoEstilosContainer"

import './PainelEdicaoInstancia.css';

class PainelEdicaoInstancia extends React.Component {

    static propTypes = {
        visivel: PropTypes.bool.isRequired,
        onClickFechar: PropTypes.func.isRequired
    };

    render() {

        const {visivel, onClickFechar} = this.props;

        if (!visivel) return null;

        return (
            <SidePanel
                titulo="Propriedades"
                exibirBotaoFechar={true}
                onClickFechar={onClickFechar}
                className="panel-propriedades">

                <TabelaEdicaoEstilosContainer />

            </SidePanel>
        );
    }

}

export default PainelEdicaoInstancia;