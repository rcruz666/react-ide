import React from 'react'
import { DragDropContext } from 'react-dnd';
import HTML5Backend from 'react-dnd-html5-backend';

import PaletaContainer from '../containers/PaletaContainer'
import CanvasContainer from '../containers/CanvasContainer'
import PainelEdicaoInstanciaContainer from '../containers/PainelEdicaoInstanciaContainer'

import './App.css';

const App = () => (
    <div className="app">
        <PaletaContainer />
        <CanvasContainer idInstanciaPai={null} />
        <PainelEdicaoInstanciaContainer />
    </div>
)

export default DragDropContext(HTML5Backend)(App);