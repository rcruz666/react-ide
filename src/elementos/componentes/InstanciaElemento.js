import React from 'react';
import PropTypes from 'prop-types';
import ReactDOM from 'react-dom';
import cx from 'react-classset';
import ImmutablePropTypes from 'react-immutable-proptypes';
import { DropTarget } from 'react-dnd';
import { ContextMenu, MenuItem, ContextMenuTrigger } from "react-contextmenu";

import {DraggableTypes} from '../dragDropConstants';
import CanvasContainer from '../containers/CanvasContainer';

import {
    selecionarDropTargetInstancia,
    selecionarDropTargetInstanciaNoTopo,
    adicionarAoCanvas,
    selecionarInstanciaParaEdicao, excluirInstancia
} from '../actions';

import './InstanciaElemento.css'
import {CSSValueToString} from "../regrasCSS";


function novaInstanciaDeveVirDepoisDaInstanciaPai(dropOffset, retanguloInstancia) {
    const metadeWidthPai = retanguloInstancia.width / 2;
    const widthDrop = dropOffset.x - retanguloInstancia.left;

    return widthDrop > metadeWidthPai;
}

function calcularPosicaoNovaIntancia(posicaoPai, deveVirDepois) {

    if (deveVirDepois) {
        return posicaoPai + 1;
    } else {
        return posicaoPai === 0 ? 0 : posicaoPai;
    }
}

const dropTarget = {
    /**
     * Quando um elemento é dropado em cima  de um canvas vazio,
     * o elemento é adicionado.
     */
    drop(props, monitor, component) {

        // Se algum filho deste componente já tratou o evento de drop,
        // não se deve fazer nada.
        if (monitor.didDrop()) return undefined;

        const dropOffset = monitor.getClientOffset();
        const instanciaPaiDomNode = ReactDOM.findDOMNode(component);
        const rectInstancia = instanciaPaiDomNode.getBoundingClientRect();

        const deveVirDepois = novaInstanciaDeveVirDepoisDaInstanciaPai(dropOffset, rectInstancia);

        let indiceNovaInstancia = calcularPosicaoNovaIntancia(props.posicao, deveVirDepois);

        props.dispatch(adicionarAoCanvas(indiceNovaInstancia));

        // Retorna um objeto para sinalizar aos possíveis DropTargets
        // acima deste que o evento de drop já foi tratado.
        return {};
    },

    hover(props, monitor, component) {

        const isHoveringOnlyOnSelf = monitor.isOver({ shallow: true });

        // Se o usuário está arrastando sobre os filhos desta instância, retorna
        if (!isHoveringOnlyOnSelf) return;

        // ou se esta instância já está registrada como o dropTarget atual, apenas retornar sem fazer nada.
        if (props.dropTargetSelecionado
            && props.dropTargetSelecionado.get('idInstancia') === props.idInstanciaElemento) {
            return false;
        }

        if (props.idInstanciaPai) {
            props.dispatch(selecionarDropTargetInstancia(props.idInstanciaElemento, props.idInstanciaPai, 0));
        } else {
            props.dispatch(selecionarDropTargetInstanciaNoTopo(props.idInstanciaElemento, 0));
        }
    }
};

function collect(connect, monitor) {
    return {
        connectDropTarget: connect.dropTarget(),
        isOver: monitor.isOver()
    };
}

class InstanciaElemento extends React.Component {

    static propTypes = {
        idInstanciaElemento: PropTypes.string,
        instanciaElemento: ImmutablePropTypes.map,
        dropTargetSelecionado: ImmutablePropTypes.map,
        posicao: PropTypes.number.isRequired,
        emEdicao: PropTypes.bool.isRequired,

        // Se não vier preenchido, significa que esta instância de elemento
        // está no topo do canvas
        // Se vier preenchido, significa que esta instância de elemento
        // está aninhado em uma outra instância
        idInstanciaPai: PropTypes.string,

        // react-dnd
        connectDropTarget: PropTypes.func.isRequired,
        isOver: PropTypes.bool.isRequired,
    };

    _handleClick = (e) => {
        e.stopPropagation();
        this.props.dispatch( selecionarInstanciaParaEdicao( this.props.idInstanciaElemento ) );
    };

    _handleClickExcluir = () => {
        this.props.dispatch(excluirInstancia(this.props.instanciaElemento.get('idInstancia')));
    };

    render() {

        const estiloInstancia = this.props.instanciaElemento
            .get('estilo')
            .map(CSSValueToString)
            .toObject();

        console.log(estiloInstancia);

        let styles = estiloInstancia;
        const isContainer = this.props.instanciaElemento.getIn(['elemento', 'container']);

        const classNames = cx({
            "instancia-elemento": true,
            "container": isContainer,
            "em-edicao": this.props.emEdicao
        });

        let instancia;

        if (isContainer) {

            // Estas regras de estilo devem sempre
            // ser aplicadas ao container
            const estilosOverrideParaContainer = {
                display: "flex",
                alignItems: 'stretch' // Para que o canvas interno da instância fique visível
            };

            styles = {...styles, ...estilosOverrideParaContainer};

            instancia = this.props.connectDropTarget(
                <div style={styles} className={classNames} onClick={this._handleClick}>
                    <div className="instancia-elemento-title">
                        {this.props.instanciaElemento.get('tipoElemento')}
                    </div>

                    <CanvasContainer style={styles} idInstanciaPai={this.props.idInstanciaElemento}/>

                </div>
            );

        } else {
            const elemento = this.props.instanciaElemento.get('elemento');
            instancia =  React.createElement( elemento.get('tag'), {
                ...elemento.get('props').toJS(),
                style: styles,
                className: classNames,
                onClick: this._handleClick
            });
        }

        const idMenu = `instancia-elemento-${this.props.instanciaElemento.get('idInstancia')}-menu`;

        return (
            <div style={styles}>
                <ContextMenuTrigger id={idMenu}>
                    {instancia}
                </ContextMenuTrigger>
                <ContextMenu id={idMenu}>
                    <MenuItem data={{}} onClick={this._handleClickExcluir}>
                        Excluir
                    </MenuItem>
                </ContextMenu>
            </div>
        );
    }
}

/**
 * Uma instância de elemento é um drop target. Quando houver um elemento sobre ela, um placeholder será
 * inserido à sua direita ou à sua esquerda, dependendo da posição do elemento sobre a instância
 * (se o elemento estiver mais à esquerda da instância, o placeholder será inserido à esquerda,
 * se o elemento estiver mais à direita da instância, o placeholder será inserido à direita).
 * Se o elemento for arrastado para o meio da instância, dentro do canvas dela, o placeholder irá
 * seguir este comportamento não mais para a instância, mas para as instâncias dentro do canvas dela.
 * Ou seja, para inserir uma instância irmã, é preciso arrastar o componente nas bordas
 * esquerda ou direita da instância. Se for arrastado para o meio da instância, ela virará uma
 * instância filha, que pode ser irmã de filhas já previamente colocadas lá.
 */
export default DropTarget(
    DraggableTypes.ELEMENTO,
    dropTarget,
    collect
)(InstanciaElemento);