import React from 'react';
import PropTypes from 'prop-types';
import ImmutablePropTypes from 'react-immutable-proptypes';
import cx from 'react-classset';
import { DropTarget } from 'react-dnd';

import ElementPlaceholder from './ElementPlaceholder'

import {
    selecionarDropTargetCanvasVazioInstancia,
    selecionarDropTargetCanvasVazioPrincipal,
    adicionarAoCanvas
} from '../actions'
import InstanciaElementoContainer from '../containers/InstanciaElementoContainer';
import {DraggableTypes} from '../dragDropConstants'

import './Canvas.css'



const dropTarget = {
    /**
     * Quando um elemento é dropado em cima  de um canvas vazio,
     * o elemento é adicionado.
     */
    drop(props, monitor) {
        props.dispatch(adicionarAoCanvas())
    },

    hover(props, monitor, component) {

        const isHoveringOnlyOnSelf = monitor.isOver({ shallow: true });

        if (!isHoveringOnlyOnSelf) return;

        if (props.idInstanciaPai) {
            if (!props.dropTargetSelecionado
                || props.dropTargetSelecionado.get('idInstanciaPai') !== props.idInstanciaPai) {
                props.dispatch(selecionarDropTargetCanvasVazioInstancia(props.idInstanciaPai))
            }
        } else {
            if (!props.dropTargetSelecionado) {
                props.dispatch(selecionarDropTargetCanvasVazioPrincipal())
            }
        }
    }
};


function collect(connect, monitor) {
    return {
        connectDropTarget: connect.dropTarget(),
        isOver: monitor.isOver()
    };
}

class Canvas extends React.Component {

    static propTypes = {
        idsInstanciasElementos: ImmutablePropTypes.listOf(PropTypes.string),

        // Se for nulo, significa que este é o canvas no topo
        // Se vier preenchido, significa que este é um canvas dentro de uma
        // instância de elemento
        idInstanciaPai: PropTypes.string,
        dropTargetSelecionado: ImmutablePropTypes.map,

        style: PropTypes.object,

        // connect
        dispatch: PropTypes.func.isRequired,

        // react dnd
        connectDropTarget: PropTypes.func,
        isOver: PropTypes.bool,
    };

    render() {

        const canvasVazio = this.props.idsInstanciasElementos.size === 0;

        const canvas = (
            <div
                className={cx({
                    'canvas': true,
                    'canvas-vazio': canvasVazio
                })}
                style={this.props.style}
            >
                {canvasVazio && this.props.isOver &&
                    <ElementPlaceholder />
                }
                {this.props.idsInstanciasElementos.map((idInstanciaElemento, indiceArray) =>
                    <InstanciaElementoContainer
                        key={idInstanciaElemento}
                        posicao={indiceArray}
                        idInstanciaPai={this.props.idInstanciaPai}
                        idInstanciaElemento={idInstanciaElemento}/>
                )}
            </div>

        );

        if (canvasVazio) {
            return this.props.connectDropTarget(canvas);
        }

        return canvas;
    }
}

/**
 * O canvas vazio é um drop target. Quando houver um elemento sobre ele, um placeholder aparecerá dentro dele.
 */
export default DropTarget(
    DraggableTypes.ELEMENTO,
    dropTarget,
    collect
)(Canvas);