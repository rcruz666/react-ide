import React from 'react';
import PropTypes from 'prop-types';
import { DragSource } from 'react-dnd';
import {DraggableTypes} from '../dragDropConstants'
import cx from 'react-classset';
import './Elemento.css'

const elementoDragSource = {
    beginDrag(props) {

        props.selecionar(props.elemento);

        return {
            name: props.elemento.get('name')
        }
    }
}

function collectDragProps(connect, monitor) {
    return {
        connectDragSource: connect.dragSource(),
        isDragging: monitor.isDragging()
    }
}

class Elemento extends React.Component {

    static propTypes = {
        elemento: PropTypes.object.isRequired,
        selecionar: PropTypes.func.isRequired
    };

    render() {

        const {isDragging} = this.props;

        return (
            <div
                className={cx({
                    "elemento-container": true,
                    "dragging": isDragging
                })}
                style={{opacity: isDragging ? 0.5 : 1}}
            >
                {
                    this.props.connectDragSource(
                        <div className="elemento elevated white-background primary-text-color">
                            {this.props.elemento.get('name')}
                        </div>
                    )
                }
                <hr className="primary-separator divider-color" />
            </div>
        )
    }
}

export default DragSource(DraggableTypes.ELEMENTO, elementoDragSource, collectDragProps)(Elemento);