import React from 'react'

import './ElementPlaceholder.css';

class ElementPlaceholder extends React.Component {

    static propTypes = {

    };

    render() {
        return (
            <div className="element-placeholder">
                <h2 className="arraste-aqui secondary-text-color">Arraste aqui</h2>
            </div>
        )
    }
}

export default ElementPlaceholder;