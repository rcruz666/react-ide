import React from 'react'
import ElementoContainer from '../containers/ElementoContainer'
import SidePanel from "../../basic-ui/componentes/SidePanel";

const Paleta = ({nomesElementos}) => {
    return (
        <SidePanel
            className="paleta"
            titulo="Paleta de Elementos">

            <div>
                {nomesElementos.map(nomeElemento =>
                    <ElementoContainer key={nomeElemento} nomeElemento={nomeElemento} />
                )}
            </div>

        </SidePanel>

    )
};

export default Paleta