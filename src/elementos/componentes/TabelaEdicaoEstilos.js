import React, {Component} from 'react';
import PropTypes from 'prop-types';
import ImmutablePropTypes from 'react-immutable-proptypes';
import PanelCard from "../../basic-ui/componentes/PanelCard";
import {criarValorParaRegra, CSSValueValor, regrasCSS} from "../regrasCSS";
import PropertyTable from "../../basic-ui/componentes/PropertyTable";
import Botao from "../../basic-ui/componentes/Botao";


class TabelaEdicaoEstilos extends Component {

    static propTypes = {
        instancia: ImmutablePropTypes.map.isRequired,
        onChangeEstilo: PropTypes.func.isRequired
    };

    render() {
        const {instancia} = this.props;

        if (!instancia) return null;

        const estilosInstancia = instancia.get('estilo');
        let propriedades = [];

        if (estilosInstancia.size > 0) {
            propriedades = Object.entries(estilosInstancia.toJS()).map(([id, valueEstiloInstancia]) => {

                const regraCSS = regrasCSS.find(r => r.id === id);

                if (!regraCSS) {
                    throw new Error("A regra " + id + " não está definida nas regras de css");
                }

                const seletorName = this._renderSeletorRegrasCSS(id);
                let seletorValue;

                if (regraCSS.componente) {
                    seletorValue = this._renderSeletorComponente(
                        instancia, valueEstiloInstancia, id, regraCSS);
                }

                if (regraCSS.valoresPossiveis) {
                    seletorValue = this._renderSeletorSimples(
                        instancia, valueEstiloInstancia, id, regraCSS.valoresPossiveis);
                }

                return {
                    id,
                    seletorName,
                    seletorValue
                }
            });
        }

        return (
            <PanelCard title="Estilos">
                <PropertyTable propriedades={propriedades}/>
                {
                    (!estilosInstancia || (estilosInstancia.size < regrasCSS.length)) &&
                    <Botao onClick={this._handleAdicionarEstilo} label="+" />
                }
            </PanelCard>
        );
    }

    _renderSeletorComponente = ( instancia, valueEstiloInstancia, idRegraCSS, regraCSS ) => {


        let componentProps = {
            ...regraCSS.componente.props,
            onChange: regraCSS.componente.makeOnChange
                ? regraCSS.componente.makeOnChange(value => this._handleChangeEstilo(
                    instancia.get('idInstancia'), idRegraCSS, value))
                : ({target: {value}}) => this._handleChangeEstilo(
                    instancia.get('idInstancia'), idRegraCSS, value)
        };

        let value = null;
        if (valueEstiloInstancia) {
            value = CSSValueValor(valueEstiloInstancia) ;
        } else if (regraCSS.defaultValue) {
            value = regraCSS.defaultValue;
        }

        if (regraCSS.componente.valuePropName) {
            componentProps[regraCSS.componente.valuePropName] = value || '';
        } else {
            componentProps.value = value || '';
        }

        return React.createElement(regraCSS.componente.nome, componentProps);
    };

    _renderSeletorSimples = (instancia, valueEstiloInstancia, idRegraCSS, valoresPossiveisRegraCSS) => {
        return (
            <select value={valueEstiloInstancia || ''}
                    onChange={({target: {value}}) => this._handleChangeEstilo(
                        instancia.get('idInstancia'), idRegraCSS, value)}>

                <option value={null}>Selecione</option>

                {valoresPossiveisRegraCSS.map(valor =>
                    <option key={valor} value={valor}>{valor}</option>)}

            </select>
        );
    };

    _handleAdicionarEstilo = () => {

        let regraCSS = null;
        let valorEstilo = null;

        const estiloInstancia = this.props.instancia.get('estilo');

        if (!estiloInstancia) {
            regraCSS = regrasCSS[0];
        } else {
            const keysEstiloInstancia = Object.keys( estiloInstancia.toJS() );
            regraCSS = regrasCSS.filter(r => !keysEstiloInstancia.includes(r.id))[0];
        }

        if (regraCSS.valoresPossiveis) {
            valorEstilo = regraCSS.valoresPossiveis[0];
        } else if (regraCSS.defaultValue) {
            valorEstilo = regraCSS.defaultValue;
        }

        this.props.onChangeEstilo(this.props.instancia.get('idInstancia'), regraCSS.id, valorEstilo);
    };

    _handleChangeEstilo = (idInstancia, idRegraCSS, valorEstilo) => {
        debugger;
        const regra = regrasCSS.find(x => x.id === idRegraCSS);

        this.props.onChangeEstilo(idInstancia, idRegraCSS, criarValorParaRegra(regra, valorEstilo));
    };

    _handleChangeSeletorRegra = (idRegraAnterior, idNovaRegra) => {
        const novaRegra = regrasCSS.find(x => x.id === idNovaRegra);

        let idInstancia = this.props.instancia.get('idInstancia');

        this.props.onChangeEstilo(
            idInstancia,
            idRegraAnterior,
            null
        );

        this.props.onChangeEstilo(
            idInstancia,
            idNovaRegra,
            novaRegra.defaultValue || novaRegra.valoresPossiveis[0]
        )
    };

    _renderSeletorRegrasCSS = (idRegraSelecionada) => {
        const idsRegrasInstancia = this.props.instancia.get('estilo').keySeq().toJS();

        const regras = regrasCSS.filter(regra => regra.id === idRegraSelecionada
            || !idsRegrasInstancia.includes(regra.id));

        return (
            <select value={idRegraSelecionada}
                    onChange={e => this._handleChangeSeletorRegra(idRegraSelecionada, e.target.value)}>
                {regras.map(regra =>
                    <option key={regra.id} value={regra.id}>
                        {regra.id}
                    </option>
                )}
            </select>
        )
    };
}

export default TabelaEdicaoEstilos;
