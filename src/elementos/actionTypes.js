
// Esta ação é disparada quando o usuário clica em um
// elemento na paleta de elementos. O elemento se torna
// selecionado e pode ser inserido no canvas de elementos
export const SELECIONAR_ELEMENTO = 'react-ide/elementos/SELECIONAR_ELEMENTO';

// Esta ação é disparada quando o usuário insere um elemento
// no canvas.
//
// No canvas há duas regiões em que elementos podem ser inseridos:
// no array mais externo de elementos, e nos arrays de children
// de cada um dos elementos já inseridos no canvas.
export const ADICIONAR_ELEMENTO = 'react-ide/elementos/ADICIONAR_ELEMENTO';

// Esta ação é disparada quando o usuário exclui um elemento do canvas.
export const EXCLUIR_INSTANCIA = 'react-ide/elementos/EXCLUIR_INSTANCIA';

// Esta ação é disparada quando o usuário arrasta um elemento por sobre
// algum item da tela que seja um drop target (ou seja, em que seja possível
// dropar o elemento arrastado)
export const SELECIONAR_DROP_TARGET = 'react-ide/elementos/SELECIONAR_DROP_TARGET';

// Esta ação é disparada quando o usuário clica em alguma instância dentro do canvas.
// A instância clicada ficará em evidência e suas propriedades poderão ser editadas 
// num painel lateral
export const SELECIONAR_INSTANCIA_PARA_EDICAO = 'react-ide/elementos/SELECIONAR_INSTANCIA_PARA_EDICAO';

// Esta ação é disparada quando o usuário fecha o painel de edição de instâncias
export const CANCELAR_EDICAO_INSTANCIA = 'react-ide/elementos/CANCELAR_EDICAO_INSTANCIA';

// Esta ação é disparada quando o usuário troca alguma regra de estilo para o componente
// em edição
export const SETAR_ESTILO_INSTANCIA_EM_EDICAO = 'react-ide/elementos/SETAR_ESTILO_INTSANCIA_EM_EDICAO';