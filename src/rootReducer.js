import { combineReducers } from 'redux-immutable';
import elementos from './elementos/reducer';

export default combineReducers({
    elementos: elementos
});